import React from 'react';
import {View, StyleSheet, TouchableOpacity} from "react-native";
import Icon from "./Icon";
import AppText from "./AppText";

//PickerItem
const CategoryPickerItem = ({item, onPress}) => {
    return (
        <TouchableOpacity style={styles.container} onPress={onPress}>
            <Icon backgroundColor={item.backgroundColor} name={item.icon} size={80}/>
            <AppText style={styles.text}>{item.label}</AppText>
        </TouchableOpacity>

    );
};

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 5,
        paddingVertical: 15,
        alignItems: 'center',
        width: '33%',
    },

    text: {
        marginTop: 5,
        textAlign: 'center',
    },
})

export default CategoryPickerItem;