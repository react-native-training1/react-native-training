import React from 'react';
import { StyleSheet, View, Image } from "react-native";
import {MaterialCommunityIcons} from '@expo/vector-icons'

import colors from '../config/colors';

const HomeScreen = () => {
    return (
        <View style={styles.container}>
            <View style={styles.closeIcon}>
                <MaterialCommunityIcons name="close" color="white" size={30}/>
            </View>

            <View style={styles.deleteIcon}>
                <MaterialCommunityIcons name="trash-can-outline" color="white" size={35}/>
            </View>
            <Image style={styles.imgBackground} source={require('../assets/chair.jpg')}>

            </Image>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.black,
        justifyContent: 'center'

    },

    imgBackground: {
        resizeMode: 'contain',
        width: '100%',

    },

    closeIcon: {
        position: 'absolute',
        top: 40,
        left: 30,
    },

    deleteIcon: {
        position: 'absolute',
        top: 40,
        right: 30,
    },
})


export default HomeScreen;
